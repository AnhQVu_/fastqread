import sys
import os
import boto.ec2
import boto.sqs
import json
import time
from subprocess import call
import boto.ec2
import tempfile
import collections
import matplotlib.pyplot as plt
import shutil
def connect_to_ec2(AWS_REGION, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY):
  '''This function has several functionalities: 
      - Invoke EC2 instances listed in ec2.config file
      - Call fabric file (fabfile.py) to connect to ec2 instances and run analysis in parallel
      - Turns off instances when analysis is done
   Inputs: 
      - AWS_REGION            : aws region
      - AWS_ACCESS_KEY_ID     : aws credentials
      - AWS_SECRET_ACCESS_KEY : aws credentials
   Outputs:
      - lcl_tmp_dir : temporary directory to save all result files
   Assumptions: 
      - script is running in 'fastqread' directory
      - There are only 2 or 4 SQS messages
   '''
   
  #read config json file to find out which ec2 instances to start
  try:
    with open(os.path.abspath('ec2.config'),'r') as f:
      config = json.load(f)
    
    instanceID = []
    for i in range(1,len(config)+1):
      instanceID.append(config[str(i)]['instanceID'])
      
  except Exception as e:
    print 'Failed to read ec2.json: ' + str(e.message)
    sys.exit(1)
    
  #connect to ec2
  boto_ec2 = boto.ec2.connect_to_region(AWS_REGION, aws_access_key_id = AWS_ACCESS_KEY_ID,
                                        aws_secret_access_key = AWS_SECRET_ACCESS_KEY)
  reservations = boto_ec2.get_all_reservations()

  #exit if no instances are found
  if reservations == []:
    raise ValueError('No Instances Found')
    
  #list available instances
  print 'Avaiable Instances...'
  ec2_list = []
  for reservation in reservations:
    print('  ->{}: {}'.format(AWS_REGION,str(reservation.instances)))
    ec2_list.append(str(reservation.instances))
  
  #list instances to run  
  running_instances = []
  for i in range(1,len(config)+1):
    for inst in ec2_list:
      if config[str(i)]['instanceID'] in inst:
        print 'Starting Instance: ' + inst
        boto_ec2.start_instances(config[str(i)]['instanceID'])
        running_instances.append(inst)
  
  #Allows enough time for instances to start
  print 'Waiting for instances to start...'     
  time.sleep(30)
    
  print('# of Running Instances: {} out of {}'.format(str(len(running_instances)),str(len(reservations))))

  #make temporary folder to save files from analysis 
  lcl_tmp_dir = tempfile.mkdtemp()
  print('Local Temporary Directory: {}'.format(lcl_tmp_dir))
  
  #call fabric to run scripts in open ec2 instances
  call('fab set_hosts retrieve_queue_messages:{}'.format(lcl_tmp_dir), shell = True)
 
  #when analysis is complete, stop instances 
  for i in ec2_list:
    print 'Stopping Instance: ' + i
    for ID in instanceID:
      if ID in i:
        boto_ec2.stop_instances(ID)   

  return lcl_tmp_dir        
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def base_content(lcl_tmp_dir):
  '''This function has several functionalities: 
      - Loads files generated from scripts run in fabfile.py
      - Concatenates lists and plot results
      - Save results to current working directory
   Inputs: 
      - lcl_tmp_dir : temporary directory that contains result files
   '''

  #initialize 
  baseContent = collections.defaultdict(list)
  
  #check if files are in folder
  counter = 0
  for root, dirs, files in os.walk(lcl_tmp_dir):
    for name in files:
      counter+=1
  
  if counter == 0:
    raise ValueError('No files found in results folder')
    
  #search for jsons recursively. Files are in format 'addBaseContentByPosition_json.json'
  for root, dirs, files in os.walk(lcl_tmp_dir):
    for name in files:
      path = os.path.join(root,name)
      if 'addBaseContentByPosition' in name:
        with open(path) as f:
          tmp = json.load(f)
          baseContent['A'].append(tmp['A'])
          baseContent['T'].append(tmp['T'])
          baseContent['C'].append(tmp['C'])
          baseContent['G'].append(tmp['G'])
          baseContent['N'].append(tmp['N'])  
          
  #add lists element-wise
  total_a = [0]*len(baseContent['A'][0])
  total_t = [0]*len(baseContent['A'][0])
  total_c = [0]*len(baseContent['A'][0])
  total_g = [0]*len(baseContent['A'][0])
  total_n = [0]*len(baseContent['A'][0])

  for j in range(len(baseContent['A'][0])):
    for i in range(len(baseContent['A'])):
      if i < len(baseContent['A'])-1:
        total_a[j] = baseContent['A'][i][j] + baseContent['A'][i+1][j]
        total_t[j] = baseContent['T'][i][j] + baseContent['T'][i+1][j]
        total_c[j] = baseContent['C'][i][j] + baseContent['C'][i+1][j]
        total_g[j] = baseContent['G'][i][j] + baseContent['G'][i+1][j]
        total_n[j] = baseContent['N'][i][j] + baseContent['N'][i+1][j]
    
  #find average of concatenated list
  for i in range(len(total_a)):
    total_a[i]/=float(len(baseContent['A']))
    total_t[i]/=float(len(baseContent['T']))
    total_c[i]/=float(len(baseContent['C']))
    total_g[i]/=float(len(baseContent['G']))
    total_n[i]/=float(len(baseContent['N']))

  #create plot 
  plt.figure()
  plt.plot(range(len(total_a)),total_a)
  plt.plot(range(len(total_t)),total_t)
  plt.plot(range(len(total_c)),total_c)
  plt.plot(range(len(total_g)),total_g)
  plt.plot(range(len(total_n)),total_n)
  plt.axis([0,len(total_a),0,1])
  plt.ylabel('(Frequency of Base/Total Number of Bases) by Position')
  plt.xlabel('Position in read')
  plt.legend(('A','T','C','G','N'),loc = 'upper right')
  plt.title('Base Content by Position')
  plt.savefig(os.path.abspath('results/base_content_by_position.png'),bbox_inches = 'tight') 

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def cg_content(lcl_tmp_dir):
  '''This function has several functionalities: 
      - Loads files generated from scripts run in fabfile.py
      - Concatenates lists and plot results
      - Save results to current working directory
   Inputs: 
      - lcl_tmp_dir : temporary directory that contains result files
   '''

  #initialize 
  cgContent = []
  
  #search for jsons recursively. Files are in format 'addGCbyPosition_json.json'
  for root, dirs, files in os.walk(lcl_tmp_dir):
    for name in files:
      path = os.path.join(root,name)
      if 'addGCbyPosition' in name:
        with open(path) as f:
          tmp = json.load(f)
          cgContent.append(tmp)

  #initialize 
  total_cg = [0] * len(cgContent[0])

  #add lists element-wise
  for j in range(len(cgContent[0])):
    for i in range(len(cgContent)):
      if i < len(cgContent)-1:
        total_cg[j] = cgContent[i][j] + cgContent[i+1][j]
  
  #average      
  for i in range(len(total_cg)):
    total_cg[i] /= float(len(cgContent))

  #plot concatenated list
  plt.figure()
  plt.plot(range(len(total_cg)),total_cg)
  plt.axis([0,len(total_cg),0,1])
  plt.ylabel('(Frequency of GC/ Total Number of Bases) by Position')
  plt.xlabel('Position in read')
  plt.legend(('CG'),loc = 'upper right')
  plt.title('GC Content by Position')
  plt.savefig(os.path.abspath('results/cg_content_by_position.png'),bbox_inches = 'tight')

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def phred_content(lcl_tmp_dir):
  '''This function has several functionalities: 
      - Loads files generated from scripts run in fabfile.py
      - Concatenates lists and plot results
      - Save results to current working directory
   Inputs: 
      - lcl_tmp_dir : temporary directory that contains result files
   '''

  #initialize
  phredContent = []
  
  #search for jsons recursively. Files are in format 'addPhredbyPosition_json.json'
  for root, dirs, files in os.walk(lcl_tmp_dir):
    for name in files:
      path = os.path.join(root,name)
      if 'addPhredbyPosition' in name:
        with open(path) as f:
          tmp = json.load(f)
          phredContent.append(tmp)
  
  #initalize list to concatenate result
  total_phred = [0] * len(phredContent[0])

  #add lists element-wise
  for j in range(len(total_phred)):
    for i in range(len(phredContent)):
      if i < len(phredContent)-1:
        total_phred[j] = phredContent[i][j] + phredContent[i+1][j]

  plt.figure()    
  plt.bar(range(len(total_phred)), total_phred)
  plt.ylabel('Frequency')
  plt.xlabel('Phred Score')
  plt.title('Phred Score Distribution per Sequence')
  plt.savefig(os.path.abspath('results/phred_distribution.png'),bbox_inches = 'tight')


#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def main():
  #check if valid current command line arguments are in place
  if len(sys.argv) > 1:
    print 'Usage: python runStudy.py'
    sys.exit(1)
    
  #get credentials from config file  
  with open(os.path.abspath('aws.config'),'r') as f:
    aws = json.load(f)
  AWS_REGION = aws['AWS_REGION']
  AWS_QUEUE_NAME = aws['AWS_QUEUE_NAME']
  AWS_ACCESS_KEY_ID = aws['AWS_ACCESS_KEY_ID']
  AWS_SECRET_ACCESS_KEY = aws['AWS_SECRET_ACCESS_KEY']

  try:
    lcl_tmp_dir = connect_to_ec2(AWS_REGION, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
  except Exception as e:
    print('Error in connection to EC2 instance: {}'.format(e))
    
  try:
    base_content(lcl_tmp_dir)
  except Exception as e:
    print('Error in performing analysis on base content: {}'.format(e))
  
  try:
    cg_content(lcl_tmp_dir)
  except Exception as e:
    print('Error in performing analysis on cg content: {}'.format(e))
  
  try:
    phred_content(lcl_tmp_dir)
  except Exception as e:
    print('Error in performing analysis on phred content: {}'.format(e))
    
  #delete temp folder
  shutil.rmtree(lcl_tmp_dir)
    
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
if __name__ == '__main__':
  main()

  
