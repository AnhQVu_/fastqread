import boto
import os
import sys
import json
import boto.sqs
import time
import tempfile
import collections
from boto.ec2 import connect_to_region
from fabric.api import env
from fabric.api import parallel
from boto.s3.connection import S3Connection

#DECLARE CONSTANTS
#get credentials from config file  
with open(os.path.abspath('aws.config'),'r') as f:
  aws = json.load(f)
REGION = aws['AWS_REGION']
AWS_QUEUE_NAME = aws['AWS_QUEUE_NAME']
AWS_ACCESS_KEY_ID = aws['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = aws['AWS_SECRET_ACCESS_KEY']

#server user, normally AWS Ubuntu instances have default user "ubuntu"
env.user = 'ubuntu'

#PEM files required to access the worker EC2 instances
env.key_filename = [os.path.abspath('ec2-instance.pem')]

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
#Fabric task to set env.hosts based on region
def set_hosts():
  env.hosts = _get_public_dns(REGION)
  
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#  
#gather public dns of ec2 instances
def _get_public_dns(REGION):
  public_dns   = []
  connection   = _create_connection(REGION,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  reservations = connection.get_all_instances()
  for reservation in reservations:
    for instance in reservation.instances:
      if instance.public_dns_name!= '':
        public_dns.append(str(instance.public_dns_name))
  return public_dns
  
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#  
# Private method for getting AWS connection
def _create_connection(REGION,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY):
  print "Connecting to ", REGION

  connection = connect_to_region(region_name = REGION, 
                                 aws_access_key_id=AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key = AWS_SECRET_ACCESS_KEY)
  print 'Connection with AWS established'
  
  return connection
  
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#  
#recieves messages from sqs and performs analysis in parallel
@parallel
def retrieve_queue_messages(lcl_tmp_dir):
  '''Notes: SQS has either two or four messages. Each message will be sent to one ec2 instance'''
  
  #find queue
  sqs_connection = boto.sqs.connect_to_region(REGION, aws_access_key_id = AWS_ACCESS_KEY_ID, aws_secret_access_key = AWS_SECRET_ACCESS_KEY)
  sqs_queue = sqs_connection.get_queue(AWS_QUEUE_NAME)

  #check if queue exists
  if sqs_queue is None:
    print 'Unable to access Queue: ' + AWS_QUEUE_NAME
    sys.exit(1)

  #check for messages
  while (1):
    print 'Waiting for message...'
    sqs_message = sqs_queue.read()
      
    #if message exists, load as json and pass off to other functions
    if (sqs_message is not None):
      #load message as json
      message = json.loads(sqs_message.get_body())
      
      #load jsons from s3 (location on s3 found in variable 'message')
      path = load_json_from_s3(message,lcl_tmp_dir)
      
      #finds average of base content by position
      sequences, qualities = add_base_content_by_position(path,lcl_tmp_dir)
      
      #finds average of cg content by position
      add_GC_by_position(sequences,path,lcl_tmp_dir)
      
      #finds total frequencies of phred scores per position
      add_phred_by_position(qualities,path,lcl_tmp_dir)
      
      #delete when analysis is complete
      sqs_queue.delete_message(sqs_message)
      
    #wait some time before looking for other message
    else:
      time.sleep(30)
      #if no more messages, break loop and finish
      if sqs_message is None:
        break
    
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def load_json_from_s3(json_path,lcl_tmp_dir):
  '''This function: 
    - loads jsons from s3
  Inputs: 
    - json_path   : json with location of json in s3
    - lcl_tmp_dir : temporary directory
  Outputs:
    -local_json_path : location of json to load in subsequent functions
  '''
  #set bucket and key
  bucket = json_path['bucket']
  key = json_path['key']
  
  #connect to s3
  print '  connecting to s3'
  s3 = S3Connection(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  bucket_obj = s3.get_bucket(bucket)
  bucket_list = bucket_obj.list(key)
  print '    -> connection complete'
  
  #initialize file download
  for key in bucket_list:
    local_json_path = os.path.abspath(os.path.join(lcl_tmp_dir,key.name.split('/')[-1]))
    key.get_contents_to_filename(local_json_path)
    
  return local_json_path
  
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def add_base_content_by_position(local_json_path,lcl_tmp_dir):
  '''This function: 
      - Counts number of bases per position and averages the list element-wise
   Inputs: 
      - local_json_path   : json to load
      - lcl_tmp_dir       : temporary directory
   Outputs:
      - sequence : List of reads. Is a n x m list where n is the number of reads in the json, and m is the length of each read.
      - qualities: List of associated quality scores. Size of list is the same as the size of the 'sequences' list
   Assumptions:
      -length of sequences and qualities list are equal
   '''
  
  with open(local_json_path) as f:
    fastq = json.load(f)
  
  #initialize
  sequences  = []
  qualities = []
   
  #index through json and append sequences and qualities to their respective lists
  for idx in range(1,len(fastq)+1):
    sequences.append(fastq[str(idx)]['sequence'])  
    qualities.append(fastq[str(idx)]['qualities'])
    
  #checks to see if sequences and qualities have same lengths
  if len(sequences) != len(qualities):
    raise ValueError('reads do not match')  
    
  #initalize list to contain counts of each base perposition
  baseA = [0]*len(sequences[0])
  baseT = [0]*len(sequences[0])
  baseC = [0]*len(sequences[0])
  baseG = [0]*len(sequences[0])
  baseN = [0]*len(sequences[0])
  
  #count
  for read in sequences:
    for i in range(len(read)):
      if read[i] == 'A':
        baseA[i] += 1
      if read[i] == 'T':
        baseT[i] += 1
      if read[i] == 'C':
        baseC[i] += 1
      if read[i] == 'G':
        baseG[i] += 1
      if read[i] == 'N':
        baseN[i] += 1
  
  
  #build json to return to be concatenated with other lists outputted from other instances 
  baseContent = collections.defaultdict(list)
 
  for total in range(len(sequences[0])):
    baseContent['A'].append(baseA[total])
    baseContent['T'].append(baseT[total])
    baseContent['C'].append(baseC[total])
    baseContent['G'].append(baseG[total])
    baseContent['N'].append(baseN[total])
    
  #find elements of lists element-wise
  for i in range(len(sequences[0])):
    baseContent['A'][i] /= float(len(sequences))
    baseContent['T'][i] /= float(len(sequences))
    baseContent['C'][i] /= float(len(sequences))
    baseContent['G'][i] /= float(len(sequences))
    baseContent['N'][i] /= float(len(sequences))
  
  #create json in local temporary directory    
  fields = local_json_path.split('/')
  local_filepath = lcl_tmp_dir+'/addBaseContentByPosition_'+fields[-1]
  
  with open(local_filepath,'w') as f:
    json.dump(baseContent,f)
      
  return sequences, qualities

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def add_GC_by_position(sequences,local_json_path,lcl_tmp_dir):
  '''This function: 
      - Counts number of C and G's per position and finds the average per position
   Inputs: 
      - sequences       : List of reads. Is a nxm list where n is the number of reads in the json, and m is the length of each read.
      - local_json_path : temporary directory
      - lcl-tmp_dir     : temporary directory
   '''
  
  #initialize gc list
  gc = [0] * len(sequences[0])
  
  #count gc content
  for read in sequences:
    for i in range(len(read)):
      if read[i] == 'C' or read[i] == 'G':
        gc[i]+=1
  
  #find average of gc content
  for i in range(len(gc)):
    gc[i] /= float(len(sequences))
   
  # build json to be concatenated with other results 
  gcContent = collections.defaultdict(list)  
  for total in range(len(sequences[0])):
    gcContent['1'].append(gc[total])
  
  #save json to local temporary directory
  fields = local_json_path.split('/')
  local_filepath = lcl_tmp_dir+'/addGCbyPosition_'+fields[-1]
  
  with open(local_filepath,'w') as f:
    json.dump(gc,f)

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def add_phred_by_position(qualities,local_json_path,lcl_tmp_dir):
  '''This function: 
      - Counts frequencies of phred quality scores per base
   Inputs: 
      - qualities         : List of quality scores. Is a n x m list where n is the number of reads in the json, and m is the length of each read.
      - local_json_path   : json to load
      - lcl_tmp_dir       : temporary directory
   Assumptions:
      -phred scoring is done using the sanger method. Can change to change scoring versions (illumina, solexa, etc) based on user input
   '''
  #x axis of history represents phred scores
  hist = [0] * 50
   
  # add frequences of phred quality scores per base
  for read in qualities:
    for phred in read:     
      q = ord(phred)-33
      hist[q] += 1

  #build json to concatenate later
  phred = collections.defaultdict(list)
  for base in range(len(qualities[0])):
    phred['1'].append(hist[base])

  #save to local temporary directory
  fields = local_json_path.split('/')
  local_filepath = lcl_tmp_dir+'/addPhredbyPosition_'+fields[-1] 

  with open(local_filepath,'w') as f:
    json.dump(hist,f)
    





