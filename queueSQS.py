import boto
import json
import sys
import os
import math
import collections
from boto.sqs.message import RawMessage
from boto.sqs.connection import SQSConnection
from boto.s3.connection import S3Connection


def generate_JSON(filepath):
  '''This function has several functionalities: 
        - Determines if fastq file is standard 4-line read format, if not will throw an error
        - If fastq file is less than a certain threshold, it will split the fastq file into two 
          separate jsons. If it is greater, fastq file will be split into four json files. 
          Saves jsons under 'results' folder
        - Write summary to text file
        - Returns boolean variables to determine how many EC2 instances to employ 
     Inputs: 
        -filepath: location of fastq file 
     Outputs: 
        -twoEC2: If true, will invoke two instances
        -fourEC2: If true, will invoke four instances
     Assumptions: 
        -fastq file is standard 4-lines per read
        -working in directory 'fastqread'
     '''
  
  #count total number of lines
  with open(filepath, 'r') as f:        
    lineCount = 0    
    for line in f: 
      lineCount+=1
   
  #check if fastq read has 4 lines per read
  if lineCount%4 != 0:
    raise ValueError('Must provide fastq file that contains 4 lines per read')
  
  #calculate total number of reads in fastq file
  reads = lineCount/4
  

  #initialize boolean
  twoEC2 = False
  fourEC2 = False
  
  #if line count is below a threshold, split fastq into two jsons
  if lineCount < 500000:
    twoEC2 = True    
    
    #initialize dictionaries
    json_1 = {}
    json_2 = {}

    #open file and split fastq in half
    with open(filepath, 'r') as f:
      counter = 0  
      counter_2  = 0  
      while True:
        counter+=1          
        if counter <= reads/2:
          json_1[counter] = {}
          f.readline().rstrip
          json_1[counter]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_1[counter]['qualities'] =f.readline().rstrip()
                  
        if counter > reads/2 and counter<=reads:
          counter_2 += 1
          json_2[counter_2] = {}
          f.readline().rstrip
          json_2[counter_2]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_2[counter_2]['qualities'] = f.readline().rstrip()
          if counter == reads:
            break 
    

    #save messages to results folder
    with open(os.path.abspath('results/json/json1.json'),'w') as outfile:
      json.dump(json_1,outfile) 
    with open(os.path.abspath('results/json/json2.json'),'w') as outfile:
      json.dump(json_2,outfile) 

  #will create 4 jsons
  else:
    fourEC2 = True
    json_1 = {}
    json_2 = {}
    json_3 = {}
    json_4 = {}
    
    with open(filepath, 'r') as f:
      counter   = 0 
      counter_2 = 0
      counter_3 = 0
      counter_4 = 0   
      while True:
        counter+=1      
        
        #start to 1/4 of fastq    
        if counter <= reads/4:
          json_1[counter] = {}
          f.readline().rstrip
          json_1[counter]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_1[counter]['qualities'] =f.readline().rstrip()
         
        #1/4 to 1/2 of fastq 
        if counter > reads/4 and counter <= reads/2:
          counter_2+=1
          json_2[counter_2] = {}
          f.readline().rstrip
          json_2[counter_2]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_2[counter_2]['qualities'] =f.readline().rstrip()
        
        #1/2 to 3/4 of fastq          
        if counter > reads/2 and counter<=(reads/4 + reads/2):
          counter_3+=1
          json_3[counter_3] = {}
          f.readline().rstrip
          json_3[counter_3]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_3[counter_3]['qualities'] = f.readline().rstrip()
        
        #3/4 to end of fastq  
        if counter > (reads/4 + reads/2) and counter <= reads:
          counter_4+=1
          json_4[counter_4] = {}
          f.readline().rstrip
          json_4[counter_4]['sequence'] = f.readline().rstrip()
          f.readline().rstrip
          json_4[counter_4]['qualities'] = f.readline().rstrip()
          if counter == reads:
            break    
    
    with open(os.path.abspath('results/json/json1.json'),'w') as outfile:
      json.dump(json_1,outfile) 
    with open(os.path.abspath('results/json/json2.json'),'w') as outfile:
      json.dump(json_2,outfile)
    with open(os.path.abspath('results/json/json3.json'),'w') as outfile:
      json.dump(json_3,outfile) 
    with open(os.path.abspath('results/json/json4.json'),'w') as outfile:
      json.dump(json_4,outfile)    

  return twoEC2,fourEC2

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def import_json_to_s3(bucket_name,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY):
  '''This function: 
        -Will upload jsons saved in generate_JSON to s3 
     Inputs: 
        -bucket_name            : name of bucket in s3
        -AWS_ACCESS_KEY_ID      : aws credentials
        -AWS_SECRET_ACCESS_KEY  : aws credentials
     Outputs: 
        -key_prefix : key prefix location of json in s3 
        -bucket_obj : bucket object
     '''  
  #connect to s3
  print '  connecting to s3'
  s3 = S3Connection(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  bucket_obj = s3.get_bucket(bucket_name)
  print '    -> connection complete'
  
  #grab jsons and send to s3
  for root, dirs, files in os.walk(os.path.abspath(os.path.join('results/json'))):
    for name in files:
      jsonPath = os.path.join(root, name)
      print('Uploading: {}'.format(jsonPath))
      mp = bucket_obj.initiate_multipart_upload(jsonPath)
      
      source_size = os.stat(jsonPath).st_size
      bytes_per_chunk = 5000*1024*1024
      chunks_count = int(math.ceil(source_size / float(bytes_per_chunk)))
      
      for i in range(chunks_count):
        offset = i * bytes_per_chunk
        remaining_bytes = source_size - offset 
        bytes = min([bytes_per_chunk, remaining_bytes])
        part_num = i+1
               
        with open(jsonPath, 'r') as fp:
          fp.seek(offset)
          mp.upload_part_from_file(fp=fp, part_num=part_num, size=bytes)

      if len(mp.get_all_parts()) == chunks_count:
        mp.complete_upload()
        print "  -> File upload done"
      else:
        mp.cancel_upload()
        print "  -> File upload fail"
        sys.exit(1)
        
  key_prefix = os.path.abspath(os.path.join('results/json')) +'/'
  
  
  return key_prefix,bucket_obj
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def populate_SQS(bucket_obj,key_prefix,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_REGION,AWS_QUEUE_NAME):
  '''This function: 
        -Will search for jsons in s3 given bucket and key prefix
        -Will search for jsons and send locations on s3 to SQS
     Inputs: 
        -bucket_obj            : bucket object
        -key_prefix            : key prefix on s3
        -bucket_obj            : bucket object
        -AWS_ACCESS_KEY_ID     : aws credentials
        -AWS_SECRET_ACCESS_KEY : aws credentials
        -AWS_REGION            : aws region name
        -AWS_QUEUE_NAME        : sqs queue name  
     '''  

  #connect to sqs queue  
  sqs_connection = boto.sqs.connect_to_region(AWS_REGION,aws_access_key_id = AWS_ACCESS_KEY_ID, aws_secret_access_key = AWS_SECRET_ACCESS_KEY)
  sqs_queue = sqs_connection.get_queue(AWS_QUEUE_NAME)
  
  #connect to s3
  bucket_list = bucket_obj.list(key_prefix[1:])

  location = {}
  #list through bucket to find json location
  for key in bucket_list:
    if '.json' in key.name:
      location['bucket'] = bucket_obj.name
      location['key'] = key.name
      #send location to sqs queue
      message = RawMessage()
      message.set_body(json.dumps(location))
      sqs_queue.write(message)

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def create_ec2_config(twoEC2,fourEC2):
  '''This function: 
        - Builds json with EC2 instance information depending on whether 2 or 4 EC2 instances are to be employed
        - Saves as ec2.config file
     Inputs: 
        - twoEC2  : If true, build json with two EC2 instances
        - fourEC2 : If true, build json with four EC2 instances
     Assumptions: 
        -EC2 instances are in existence already
        -Will employ only two or four instances
     '''  
     
  ec2JSON = {}
  counter = 0
  if twoEC2:
    for i in range(2):
      counter += 1
      ec2JSON[counter] = {}
      if counter == 1:
        ec2JSON[counter]['name'] = 'instance-1'
        ec2JSON[counter]['instanceID'] = 'i-023cb91ee68c91094'
      if counter == 2:
        ec2JSON[counter]['name'] = 'instance-2'
        ec2JSON[counter]['instanceID'] = 'i-0be577b469ff69bc5'
  
  if fourEC2:
    for i in range(4):
      counter += 1
      ec2JSON[counter] = {}
      if counter == 1:
        ec2JSON[counter]['name'] = 'instance-1'
        ec2JSON[counter]['instanceID'] = 'i-023cb91ee68c91094'
      if counter == 2:
        ec2JSON[counter]['name'] = 'instance-2'
        ec2JSON[counter]['instanceID'] = 'i-0be577b469ff69bc5'
      if counter == 3:
        ec2JSON[counter]['name'] = 'instance-3'
        ec2JSON[counter]['instanceID'] = 'i-0401fe04e9ced4dbe'
      if counter == 4:
        ec2JSON[counter]['name'] = 'instance-4'
        ec2JSON[counter]['instanceID'] = 'i-0d908e79b1f46ae13'

  with open(os.path.abspath('ec2.config'),'w') as f:
    json.dump(ec2JSON,f)
      
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def summary(filepath):
  #Writes basic statistics of fastq to summary txt file
  
  #count total number of lines
  with open(filepath, 'r') as f:        
    lineCount = 0    
    for line in f: 
      lineCount+=1
   
  #calculate total number of reads in fastq file
  reads = lineCount/4
  
  with open(filepath, 'r') as f:
    
    sequences = []
    qualities = []
    #making lists of sequences and qualities
    while True:
        f.readline()
        seq = f.readline().rstrip()
        f.readline()
        qual = f.readline().rstrip()
        
        #check to see if end of line is reached
        if len(seq) == 0:
          break
          
        sequences.append(seq)
        qualities.append(qual)
      
  #count number of bases
  count = collections.Counter()
  for seq in sequences:
    count.update(seq)
  
  total_base = float(reads*len(sequences[0]))
  with open(os.path.abspath('results/{}_summary.txt'.format(filepath.split('/')[-1].split('.')[0])),'a') as a:    
    a.write('Total Number of Reads: {}\n'.format(reads))
    a.write('Length of each Read: {}\n'.format(len(sequences[0])))
    a.write('----------------------------\n')
    a.write(' Base | Total  | Percent(%)  \n')
    a.write('----------------------------\n')
    a.write('  {}     {}    {}   \n'.format('A', str(count['A']), str(100*(count['A']/total_base))))
    a.write('  {}     {}    {}   \n'.format('T', str(count['T']), str(100*(count['T']/total_base))))
    a.write('  {}     {}    {}   \n'.format('C', str(count['C']), str(100*(count['C']/total_base))))
    a.write('  {}     {}    {}   \n'.format('G', str(count['G']), str(100*(count['G']/total_base)))) 
    a.write('  {}     {}        {}    \n'.format('N', str(count['N']), str(100*(count['N']/total_base))))
    a.write('----------------------------\n')
    a.write('Phred Scoring Version: Sanger Phred+33')
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def main():
  #check if valid current command line arguments are in place
  if len(sys.argv) < 3:
    print 'Usage: python queueSQS.py <location of fastqfile> <name of aws bucket>'
    sys.exit(1)
    
  #get credentials from config file  
  with open(os.path.abspath('aws.config'),'r') as f:
    aws = json.load(f)
  AWS_REGION = aws['AWS_REGION']
  AWS_QUEUE_NAME = aws['AWS_QUEUE_NAME']
  AWS_ACCESS_KEY_ID = aws['AWS_ACCESS_KEY_ID']
  AWS_SECRET_ACCESS_KEY = aws['AWS_SECRET_ACCESS_KEY']
  
  
  
  #generates json
  try:
    twoEC2,fourEC2 = generate_JSON(sys.argv[1])
  except Exception as e:
    print 'Error creating fastq json files: ' + str(e)
    
  #write basic stats to txt summary file
  summary(sys.argv[1])  
  
  #generate ec2 config file
  try:
    create_ec2_config(twoEC2,fourEC2)
  except Exception as e:
    print 'Error creating EC2 config file: ' + str(e)
    
  #transfer json to s3
  try:
    key_prefix,bucket_obj = import_json_to_s3(sys.argv[2],AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  except Exception as e:
    print 'Error sending json to S3: ' + str(e)
   
  #populate sqs 
  try: 
    populate_SQS(bucket_obj,key_prefix,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_REGION,AWS_QUEUE_NAME)  
  except Exception as e:
    print 'Error sending messages to SQS queue: ' + str(e)


#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
if __name__ == '__main__':
  main()
  
  
   
