import unittest
import boto
import os
import json
import boto.ec2
from boto.s3.connection import S3Connection
from boto.sqs.connection import SQSConnection
from queueSQS import generate_JSON

class testConnection(unittest.TestCase):
  def setUp(self):    
    with open(os.path.abspath('aws.config'),'r') as f:
      aws = json.load(f)
      
    self.AWS_REGION = aws['AWS_REGION']
    self.AWS_QUEUE_NAME = aws['AWS_QUEUE_NAME']
    self.AWS_ACCESS_KEY_ID = aws['AWS_ACCESS_KEY_ID']
    self.AWS_SECRET_ACCESS_KEY = aws['AWS_SECRET_ACCESS_KEY']
    
  def test_bucket_connection(self):
    s3 = S3Connection(self.AWS_ACCESS_KEY_ID,self.AWS_SECRET_ACCESS_KEY)
    bucket_obj = s3.get_bucket('bioinformatics-analysis')
    self.assertIsNotNone(bucket_obj)

  def test_sqs_connection(self):
    sqs = boto.sqs.connect_to_region(self.AWS_REGION, aws_access_key_id = self.AWS_ACCESS_KEY_ID, 
                                     aws_secret_access_key = self.AWS_SECRET_ACCESS_KEY)
    queue = sqs.get_queue(self.AWS_QUEUE_NAME)
    self.assertIsNotNone(queue)
  
  def test_ec2_connection(self):
    ec2 = boto.ec2.connect_to_region(self.AWS_REGION, aws_access_key_id = self.AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key = self.AWS_SECRET_ACCESS_KEY)
    reservations = ec2.get_all_reservations()
    self.assertIsNotNone(reservations)
      
class test_queue_SQS(unittest.TestCase):
  #test with different types of fastqfiles 
  def test_generate_JSON(self):
    generate_JSON(os.path.abspath('sampleFastq/1_Cold_TESTFILE.fastq'))

    
unittest.main()

