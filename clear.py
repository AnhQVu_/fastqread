import os
import boto
import sys
import json
from boto.s3.connection import S3Connection
from boto.sqs.connection import SQSConnection

#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def clear_folder(filepath):
  for root, dirs, files in os.walk(filepath):
    for name in files:
      path = os.path.join(root,name)
      os.remove(path)
         
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def clear_s3(bucket, key_prefix,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY):
  print '  connecting to s3'
  s3 = S3Connection(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  bucket_obj = s3.get_bucket(bucket)
  print '    -> connection complete'
  
  #connect to s3
  bucket_list = bucket_obj.list(key_prefix[1:])

  for key in bucket_list:
    bucket_obj.delete_key(key)
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def purge_sqs(AWS_REGION,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_QUEUE_NAME):
  sqs = boto.sqs.connect_to_region(AWS_REGION, aws_access_key_id = AWS_ACCESS_KEY_ID, 
                                   aws_secret_access_key = AWS_SECRET_ACCESS_KEY)
  queue = sqs.get_queue(AWS_QUEUE_NAME) 
  
  sqs.purge_queue(queue)
  
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
def main():
  with open(os.path.abspath('aws.config'),'r') as f:
    aws = json.load(f)
    
  AWS_REGION = aws['AWS_REGION']
  AWS_QUEUE_NAME = aws['AWS_QUEUE_NAME']
  AWS_ACCESS_KEY_ID = aws['AWS_ACCESS_KEY_ID']
  AWS_SECRET_ACCESS_KEY = aws['AWS_SECRET_ACCESS_KEY']
  
  clear_folder(sys.argv[1])
  clear_s3(sys.argv[2],sys.argv[1],AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
  purge_sqs(AWS_REGION,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_QUEUE_NAME)
#----------------#----------------#----------------#----------------#----------------#----------------#----------------#----------------#
if __name__ == '__main__':
  main()
